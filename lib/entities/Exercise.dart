class Exercise {
  String name;
  int repetitions;
  int weight;
  int sets;
  // these two will come later
  late String notes;
  late bool isComplete;

  Exercise(
      {required this.name,
      required this.repetitions,
      required this.weight,
      required this.sets});
}

import 'package:flutter/material.dart';
import 'screens/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const FitNote());
}

class FitNote extends StatefulWidget {
  const FitNote({Key? key}) : super(key: key);

  @override
  _FitNoteState createState() => _FitNoteState();
}

class _FitNoteState extends State<FitNote> {
  final Future<FirebaseApp> fbApp = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(),
      home: FutureBuilder(
        future: fbApp,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print('You have an error! ${snapshot.error.toString()}');
            return Text('Something Went Wrong');
          } else if (snapshot.hasData) {
            return LoginScreen();
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      // LoginScreen(),
    );
  }
}

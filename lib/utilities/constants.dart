import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

InputDecoration kLoginScreenTextFieldStyle(String? hintText, IconData? icon) {
  return InputDecoration(
    hintText: hintText,
    hintStyle: TextStyle(
      color: Colors.grey,
      fontWeight: FontWeight.w600,
    ),
    fillColor: Colors.white,
    filled: true,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(20.0),
      ),
      borderSide: BorderSide.none,
    ),
    icon: FaIcon(
      icon,
      size: 30.0,
    ),
  );
}

InputDecoration kSignUpScreenTextFieldStyle() {
  return InputDecoration(
    fillColor: Colors.white,
    filled: true,
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(20.0),
      ),
      borderSide: BorderSide.none,
    ),
  );
}

Text kLoginButtonTextStyle(String action) {
  return Text(
    action,
    style: TextStyle(
      fontWeight: FontWeight.w700,
    ),
  );
}

TextStyle kSignUpPageTextStyle() {
  return TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.w700,
    fontSize: 18,
  );
}

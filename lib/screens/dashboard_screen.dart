import 'dart:math';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../widgets/add_program_bottomsheet.dart';
import '../widgets/program_tiles.dart';

// placeholder values
late List<String> program_names = [];

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  final _auth = FirebaseAuth.instance;
  late User loggedInUser;

  @override
  void initState() {
    super.initState();
    getCurrentUser();
  }

  void getCurrentUser() async {
    try {
      final user = await _auth.currentUser;
      if (user != null) {
        loggedInUser = user;
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF3A405A),
        elevation: 0,
        title: Row(
          children: [
            Image.asset(
              'assets/justlogo.png',
              height: 25,
            ),
            Text(
              'FitNote',
              style: TextStyle(fontSize: 24),
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Material(
              type: MaterialType.transparency,
              child: Ink(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(1000.0),
                  border: Border.all(color: Colors.white, width: 3.0),
                  color: Colors.transparent,
                  shape: BoxShape.rectangle,
                ),
                child: InkWell(
                  borderRadius: BorderRadius.circular(1000.0),
                  onTap: () {
                    setState(() {
                      showModalBottomSheet(
                          context: context,
                          builder: (context) => bottomSheetLayout(
                                addProgramCallback: (programName) {
                                  setState(() {
                                    program_names.add(programName);
                                  });
                                  Navigator.pop(context);
                                },
                              ));
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                    child: Row(
                      children: [
                        FaIcon(
                          FontAwesomeIcons.plus,
                          size: 17,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Add Program',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: program_names.length == 0
            ? noProgramsScreen()
            : ListView.builder(
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TaskTile(
                      programTitle: program_names[index],
                      loggedInUser: loggedInUser.email.toString(),
                    ),
                  );
                },
                itemCount: program_names.length,
              ),
      ),
    );
  }
}

Container noProgramsScreen() {
  return Container(
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'No Workout Programs Created',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w700,
              color: Color(0xFF616263),
            ),
          ),
          FaIcon(
            FontAwesomeIcons.exclamationCircle,
            color: Color(0xFF616263),
            size: 60,
          ),
        ],
      ),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class TaskTile extends StatelessWidget {
  late final String programTitle;
  late final String loggedInUser;

  TaskTile({required this.programTitle, required this.loggedInUser});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      tileColor: Colors.white,
      onTap: () {
        //TODO Create a new screen that has all the program details, exercises, etc
      },
      onLongPress: () {
        // TODO Add bottom sheet where you can edit the title or delete the program
      },
      title: Text(
        programTitle,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontSize: 24,
        ),
      ),
      subtitle: Text(
        "Created by " + loggedInUser,
        style: TextStyle(
          fontSize: 14,
          color: Colors.grey,
        ),
      ),
      trailing: FaIcon(
        FontAwesomeIcons.alignJustify,
        color: Colors.grey,
        size: 30,
      ),
    );
  }
}
